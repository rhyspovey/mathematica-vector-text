(* ::Package:: *)

(* ::Title:: *)
(*Vector Text*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Section:: *)
(*Front End*)


BeginPackage["VectorText`"];


VectorText::usage="VectorText[text,position,size] produces FilledCurve objects for text. Has options Alignment, FontFamily, FontSlant, FontWeight, \"Rotation\".";


VectorText::alignment="`1` alignment specification `2` not recognized.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


VectorText[
text_String,
position:_List:{0,0},
size:_?NumberQ:1,
opts:OptionsPattern[]
]:=VectorText[text,position,size,opts]=Module[
{curves,coords,tx,ty,alignment,size2,testcoords},
size2=If[OptionValue["RescaleSize"],2,1]size;
curves=First[First@ImportString[ExportString[
Style[text,{FontSize->size2,LineBreakWithin->False}~Join~FilterRules[{opts},Options[Style]]],
"PDF"],"PageGraphics","TextMode"->"Outlines"]][[2,1]];
(* GeometricFunctions`DecodeFilledCurve can convert number specifed FilledCurve to named *)
coords=Flatten[curves/.FilledCurve[\[FormalA]_,\[FormalB]_]:>Flatten[\[FormalB],1],1];
testcoords=Flatten[First[First@ImportString[ExportString[
Style["I",{FontSize->size2}~Join~FilterRules[{opts},Options[Style]]],
"PDF"],"PageGraphics","TextMode"->"Outlines"]][[2,1]]/.FilledCurve[\[FormalA]_,\[FormalB]_]:>Flatten[\[FormalB],1],1];

alignment=If[ListQ[OptionValue[Alignment]],OptionValue[Alignment],{OptionValue[Alignment],Center}];
tx=Switch[alignment[[1]],
None,0,
Center,Mean[MinMax[coords[[All,1]]]],
Left,Min[coords[[All,1]]],
Right,Max[coords[[All,1]]],
_?NumberQ,alignment[[1]] size2,
_,Message[VectorText::alignment,"Horizontal",alignment[[1]]];Return[$Failed]
];
ty=Switch[alignment[[2]],
None,0,
Center,Mean[MinMax[testcoords[[All,2]]]],
Bottom,Min[testcoords[[All,2]]],
Top,Max[testcoords[[All,2]]],
"AbsoluteCenter",Mean[MinMax[coords[[All,2]]]],
_?NumberQ,alignment[[2]] size2,
,Message[VectorText::alignment,"Vertical",alignment[[1]]];Return[$Failed]
];

If[OptionValue["Rotation"]===0,
Sequence@@(Translate[#,position-{tx,ty}]&/@curves),
Sequence@@(Rotate[Translate[#,position-{tx,ty}],OptionValue["Rotation"],position]&/@curves)
]

];


Options[VectorText]={
FontFamily->"Times",FontSlant->Plain,FontWeight-> Plain,
Alignment->Center,
"RescaleSize"->True,
"Rotation"->0
};


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
